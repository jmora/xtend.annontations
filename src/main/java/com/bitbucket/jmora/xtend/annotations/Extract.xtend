package com.bitbucket.jmora.xtend.annotations

import org.eclipse.xtend.lib.macro.Active
import org.eclipse.xtend.lib.macro.AbstractClassProcessor
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.RegisterGlobalsContext
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.Visibility
import org.eclipse.xtend.lib.macro.CodeGenerationContext
import java.util.List

@Active(ExtractProcessor)
annotation Extract {

	class ExtractProcessor extends AbstractClassProcessor {

		override doRegisterGlobals(ClassDeclaration annotatedClass, RegisterGlobalsContext context) {
			context.registerInterface(annotatedClass.interfaceName)
		}

		def getInterfaceName(ClassDeclaration annotatedClass) {
			annotatedClass.qualifiedName.substring(0, annotatedClass.qualifiedName.length - 4);
		}

		override doTransform(MutableClassDeclaration annotatedClass, extension TransformationContext context) {
			val interfaceType = findInterface(annotatedClass.interfaceName)

			// add the interface to the list of implemented interfaces
			annotatedClass.implementedInterfaces = annotatedClass.implementedInterfaces +
				#[interfaceType.newTypeReference]

			// add the public methods to the interface
			for (method : annotatedClass.declaredMethods) {
				if (method.visibility == Visibility.PUBLIC) {
					interfaceType.addMethod(method.simpleName) [
						docComment = method.docComment
						returnType = method.returnType
						for (p : method.parameters) {
							addParameter(p.simpleName, p.type)
						}
						exceptions = method.exceptions
					]
				}
			}
		}

		override doGenerateCode(List<? extends ClassDeclaration> annotatedSourceElements,
			CodeGenerationContext context) {
			for (clazz : annotatedSourceElements) {
				val filePath = clazz.compilationUnit.filePath
				val file = context.getTargetFolder(filePath).append(clazz.qualifiedName.replace('.', '/') +
					".properties")
				context.setContents(file, '''
					«FOR field : clazz.declaredFields»
						«field.simpleName» 
					«ENDFOR»
				''')
			}
		}
	}

}